# README #

* JSON Rest API
* JPA with Hibernate
* Generic API controller using path variabels to avoid a controller per entitie
* Services are bound to generic controller. When a Entitie apple is used, we will reuire a bean AppleSerive, in order to call /api/apple/1
* Entities require JPARespostiories and Services
* Uniform Message for data exchange 

# Entities extend BaseEntity and require annotation DvbJsonApiName
	  @DvbJsonApiName("locatie")
      @Entity
      @Table(
              name="locaties",
              uniqueConstraints=
              @UniqueConstraint(columnNames={"postcode", "nummer", "toevoeging"})
      )
      public class Locaties extends BaseEntity  {

# Uniform Message for data exchange #
        {
          "data" : {
            "id" : "a3e99488-5d76-3bef-affd-4ca4d4073f3f",
            "type" : "person",
            "attributes" : {
              "year" : 0,
              "name" : "Ben Affleck",
              "version" : 0
            },
            "relationships" : {
              "roles" : {
                "links" : {
                  "self" : "http://localhost:8080/api/person/a3e99488-5d76-3bef-affd-4ca4d4073f3f/relationships/roles",
                  "related" : "http://localhost:8080/api/person/a3e99488-5d76-3bef-affd-4ca4d4073f3f/roles"
                }
              }
            },
            "links" : {
              "self" : "http://localhost:8080/api/person/a3e99488-5d76-3bef-affd-4ca4d4073f3f"
            }
          }
        }

# http://localhost:8080 Info supported API Calls #           
        {
          "entities" : {
            "locatie" : "http://localhost:8080/api/locatie",
            "movie" : "http://localhost:8080/api/movie",
            "person" : "http://localhost:8080/api/person",
            "role" : "http://localhost:8080/api/role",
            "schedule" : "http://localhost:8080/api/schedule"
          },
          "options" : {
            "DELETE /api/{entity}/id" : "Delete 1 entity item",
            "GET  /api/{entity}/id" : "Fetch 1 entity item",
            "GET  /api/{entity}?param0=x,param1=y" : "Fetch List of items based on params",
            "PATCH /api/{entity}/id" : "Update 1 entity item",
            "POST /api/{entity}" : "Add 1 entity item"
          },
          "resource-info" : "http://localhost:8080/"
        }

         
# FOR JDK 11 update:
        <!-- EXTRA UPDATES FOR JAVA 11  -->
        <dependency>
            <groupId>javax.xml.bind</groupId>
            <artifactId>jaxb-api</artifactId>
            <version>2.2.11</version>
        </dependency>
        <dependency>
            <groupId>javax.activation</groupId>
            <artifactId>activation</artifactId>
            <version>1.1.1</version>
        </dependency>
        <!-- https://mvnrepository.com/artifact/org.javassist/javassist -->
        <!-- update Hibernate dependency on Javassist to WORK WI|TH JAVA 11
           to 3.23.1 for Java 11 compatibility -->
        <dependency>
           <groupId>org.javassist</groupId>
            <artifactId>javassist</artifactId>
            <version>3.24.0-GA</version>
        </dependency>
        <!-- EXTRA UPDATES FOR JAVA 11  END -->
               
