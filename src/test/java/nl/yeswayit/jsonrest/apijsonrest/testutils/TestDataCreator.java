package nl.yeswayit.jsonrest.apijsonrest.testutils;


import nl.yeswayit.jsonrest.data.messages.main.JsonWrappedMessage;

import java.util.ArrayList;
import java.util.List;

public class TestDataCreator {

    public static JsonWrappedMessage addLocatie1(String uri){
        JsonWrappedMessage jsonWrappedMessage = new JsonWrappedMessage();
        jsonWrappedMessage.setType("locatie");
        jsonWrappedMessage.getData().getAttributes().put("postcode","2806EB");
        jsonWrappedMessage.getData().getAttributes().put("nummer",55);
        jsonWrappedMessage.getData().getAttributes().put("woonplaats","Gouda");
        jsonWrappedMessage.getData().getAttributes().put("straatnaam","Poldermolenerf");
        String msg  = TestRequestHttpHandler.clientCallRequest(TestRequestHttpHandler.contructRequestPost(uri, jsonWrappedMessage));
        JsonWrappedMessage result = TestRequestHttpHandler.getJsonObject(msg, JsonWrappedMessage.class);
        return result;
    }

    public static JsonWrappedMessage addLocatie2(String uri){
        JsonWrappedMessage jsonWrappedMessage = new JsonWrappedMessage();
        jsonWrappedMessage.setType("locatie");
        jsonWrappedMessage.getData().getAttributes().put("postcode","2907DD");
        jsonWrappedMessage.getData().getAttributes().put("toevoeging","bis");
        jsonWrappedMessage.getData().getAttributes().put("nummer",11);
        jsonWrappedMessage.getData().getAttributes().put("woonplaats","Capelle aan den IJssel");
        jsonWrappedMessage.getData().getAttributes().put("straatnaam","Grafiek");

        String msg  = TestRequestHttpHandler.clientCallRequest(TestRequestHttpHandler.contructRequestPost(uri,
                jsonWrappedMessage));
        JsonWrappedMessage result = TestRequestHttpHandler.getJsonObject(msg, JsonWrappedMessage.class);
        return result;
    }


    public static JsonWrappedMessage addLocatie3(String uri){
        JsonWrappedMessage jsonWrappedMessage = new JsonWrappedMessage();
        jsonWrappedMessage.setType("locatie");
        jsonWrappedMessage.getData().getAttributes().put("postcode","2525KH");
        jsonWrappedMessage.getData().getAttributes().put("toevoeging","");
        jsonWrappedMessage.getData().getAttributes().put("nummer",4);
        jsonWrappedMessage.getData().getAttributes().put("woonplaats","Den Haag");
        jsonWrappedMessage.getData().getAttributes().put("straatnaam","Fruitweg");

        String msg  = TestRequestHttpHandler.clientCallRequest(TestRequestHttpHandler.contructRequestPost(uri,
                jsonWrappedMessage));
        JsonWrappedMessage result = TestRequestHttpHandler.getJsonObject(msg, JsonWrappedMessage.class);
        return result;
    }

    public static List<JsonWrappedMessage> getCreatedTestData(String uri){
        List<JsonWrappedMessage> resultList = new ArrayList<>();
        resultList.add(addLocatie1(uri));
        resultList.add(addLocatie2(uri));
        resultList.add(addLocatie3(uri));

        return resultList;
    }
}
