package nl.yeswayit.jsonrest.apijsonrest.testutils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import okhttp3.*;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpHeaders;

import java.io.IOException;
import java.text.SimpleDateFormat;

public class TestRequestHttpHandler {
    public final static TestRestTemplate restTemplate = new TestRestTemplate();
    public final static HttpHeaders headers = new HttpHeaders();

    static
    {
        headers.add("Accept", "application/json");
        headers.add("Content-Type", "application/json");

    }


    public static String getJsonAsString(Object jsonTransferData){
        ObjectMapper om = new ObjectMapper();

        om.enable(SerializationFeature.INDENT_OUTPUT);
        om.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        om.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
        om.setDateFormat(sdf);


        String jsonTransferTest = null;
        try {
            jsonTransferTest = om.writeValueAsString(jsonTransferData);
            return jsonTransferTest;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }


    public static <E> E getJsonObject(String jsonTransferData, Class<E> clazz){
        ObjectMapper om = new ObjectMapper();
        try {
            E result = om.readValue(jsonTransferData, clazz);
            return result;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String clientCallRequest(Request request){
        Response response = null;
        try {
            OkHttpClient client = new OkHttpClient();
            response = client.newCall(request).execute();
            if (response.isSuccessful()) {
                return response.body().string();
            } else {
                System.out.println(response.message());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static Request contructRequestPatch(String uri, Object jsonTransferData){
        String jsonTransferString = (jsonTransferData.getClass().equals(String.class)) ? (String)jsonTransferData : getJsonAsString(jsonTransferData);


        MediaType JSON = MediaType.parse("application/json"); //application/vnd.api+json
        RequestBody body = RequestBody.create(JSON, jsonTransferString);


        Request request = new Request.Builder()
                .url(uri)
                .patch(body) //PUT
                .addHeader("Accept", "application/json")
                .addHeader("Content-Type", "application/json")
                .build();
        return request;
    }

    public static Request contructRequestPost(String uri, Object jsonTransferData){
        String jsonTransferString = (jsonTransferData.getClass().equals(String.class)) ? (String)jsonTransferData : getJsonAsString(jsonTransferData);

        MediaType JSON = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(JSON, jsonTransferString);


        Request request = new Request.Builder()
                .url(uri)
                .post(body) //PUT
                .addHeader("Accept", "application/json")
                .addHeader("Content-Type", "application/json")
                .build();
        return request;
    }


    public static Request contructRequestGet(String uri){
        Request request = new Request.Builder()
                .url(uri)
                .get()
                .addHeader("Accept", "application/json")
                .addHeader("Content-Type", "application/json")
                .build();
        return request;
    }

    public static Request contructRequestDelete(String uri){
        Request request = new Request.Builder()
                .url(uri)
                .delete()
                .addHeader("Accept", "application/json")
                .addHeader("Content-Type", "application/json")
                .build();
        return request;
    }

}
