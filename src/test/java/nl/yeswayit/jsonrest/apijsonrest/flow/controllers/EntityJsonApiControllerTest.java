package nl.yeswayit.jsonrest.apijsonrest.flow.controllers;

import nl.yeswayit.jsonrest.apijsonrest.ApiJsonRestApplication;
import nl.yeswayit.jsonrest.apijsonrest.ApiJsonRestApplicationTests;
import nl.yeswayit.jsonrest.apijsonrest.testutils.TestDataCreator;
import nl.yeswayit.jsonrest.data.messages.error.EnvelopeError;
import nl.yeswayit.jsonrest.data.messages.error.ErrorResponse;
import nl.yeswayit.jsonrest.data.messages.error.JsonFormatError;
import nl.yeswayit.jsonrest.data.messages.main.JsonListMessage;
import nl.yeswayit.jsonrest.data.messages.main.JsonWrappedMessage;
import org.junit.*;
import org.junit.rules.ExpectedException;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Map;

import static nl.yeswayit.jsonrest.apijsonrest.testutils.TestRequestHttpHandler.*;
import static org.junit.Assert.*;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class EntityJsonApiControllerTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(ApiJsonRestApplicationTests.class);
    private static final int PORTNUMBER = 65535;
    private static final String PORTARG = "--server.port="+PORTNUMBER;
    private static final String BASE_URL = "http://localhost:"+PORTNUMBER;

    private static JsonWrappedMessage result1;
    private static JsonWrappedMessage result2;
    private static JsonWrappedMessage result3;
    private static List<JsonWrappedMessage> resultList;

    private static TestRestTemplate restTemplate = new TestRestTemplate();
    private static HttpHeaders headers = new HttpHeaders();
    static
    {
        headers.add("Accept", "application/json");
        headers.add("Content-Type", "application/json");

    }

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @BeforeClass
    public static void init(){
        String[] args = new String[]{PORTARG};
        //SpringApplication.run(ApiJsonRestApplication.class, args);
        ApiJsonRestApplication.main(args);
    }

    @Before
    public void befoteTest(){
        if (resultList == null) {
            resultList = TestDataCreator.getCreatedTestData(BASE_URL +"/api/locatie");
            result1 = resultList.get(0);
            result2 = resultList.get(1);
            result3 = resultList.get(2);
        }
    }


    @Test
    public void test000_GetDefinedID1(){
        String msg = clientCallRequest(contructRequestGet(BASE_URL));
        Map<String,Object> result = getJsonObject(msg,Map.class);
        assertNotNull(result.get("entities"));
        assertNotNull(result.get("options"));
        assertNotNull(result.get("resource-info"));
    }



    @Test
    public void test001_GetDefinedID1(){
        JsonWrappedMessage result = handleGetLocatieDefinedIdByUrl(BASE_URL +"/api/locatie/"+ result1.getId());
        assertEquals(1L, Long.valueOf(result.getId()).longValue());
    }


    @Test
    public void test001_GetDefinedID2(){
        JsonWrappedMessage result = handleGetLocatieDefinedIdByUrl(BASE_URL +"/api/locatie/"+ result2.getId());
        assertEquals(2L, Long.valueOf(result.getId()).longValue());
    }


    @Test
    public void test001_FindByParams0(){
        String result = clientCallRequest(contructRequestGet(BASE_URL +"/api/locatie?postcode=9999AZ"));
        LOGGER.info(result);
        JsonListMessage jsonListMessage = getJsonObject(result,JsonListMessage.class);
        assertNotNull(jsonListMessage);
        assertEquals(0, jsonListMessage.getData().size());
    }


    @Test
    public void test001_FindByParams1(){
        String result = clientCallRequest(contructRequestGet(BASE_URL +"/api/locatie?postcode=28"));
        LOGGER.info(result);
        JsonListMessage jsonListMessage = getJsonObject(result,JsonListMessage.class);
        assertNotNull(jsonListMessage);
        assertEquals(1, jsonListMessage.getData().size());
    }


    @Test
    public void test001_FindByParams2(){
        String result = clientCallRequest(contructRequestGet(BASE_URL +"/api/locatie?postcode=0"));
        LOGGER.info(result);
        JsonListMessage jsonListMessage = getJsonObject(result,JsonListMessage.class);
        assertNotNull(jsonListMessage);
        assertEquals(2, jsonListMessage.getData().size());
    }

    @Test
    public void test001_FindByParams3(){
        String result = clientCallRequest(contructRequestGet(BASE_URL +"/api/locatie?postcode=2"));
        LOGGER.info(result);
        JsonListMessage jsonListMessage = getJsonObject(result,JsonListMessage.class);
        assertNotNull(jsonListMessage);
        assertEquals(3, jsonListMessage.getData().size());
    }



    @Test
    public void test002_DeleteInvalidId(){
        String targetUrl = BASE_URL+ "/api/locatie/111";
        HttpEntity<String> request = new HttpEntity<String>(headers);
        ResponseEntity<String> result = null;
        result = restTemplate.exchange(targetUrl, HttpMethod.DELETE, request, String.class);
        assertEquals(200, result.getStatusCodeValue());
        assertNotNull(result.getBody());

        String msg = result.getBody();
        System.out.println(msg);

        EnvelopeError envelopeError = getJsonObject(msg, EnvelopeError.class);
        assertNotNull(envelopeError);
        assertEquals(1,envelopeError.getErrors().size());
        JsonFormatError error0 = envelopeError.getErrors().get(0);
        assertEquals("resource not found: id=111", error0.getTitle());
        assertEquals("404", error0.getStatus());
    }


    @Test
    public void test002_DeleteValidId(){
        String targetUrl = BASE_URL+ "/api/locatie/"+ result3.getId();;
        HttpEntity<String> request = new HttpEntity<String>(headers);
        ResponseEntity<String> result = null;
        result = restTemplate.exchange(targetUrl, HttpMethod.DELETE, request, String.class);
        assertEquals(204, result.getStatusCodeValue());
        assertNull(result.getBody());
    }


    @Test
    public void test003_InvalidService(){
        String targetUrl = BASE_URL+ "/api/unknown/1";;
        String msg = clientCallRequest(contructRequestGet(targetUrl));

       LOGGER.info(msg);

        EnvelopeError result = getJsonObject(msg, EnvelopeError.class);
        assertNotNull(result);
        assertEquals(1,result.getErrors().size());
        JsonFormatError error0 = result.getErrors().get(0);
        assertEquals("No bean named 'unknownService' available", error0.getTitle());
        assertEquals("500", error0.getStatus());
        assertEquals(String.format("url: http://localhost:%d/api/unknown/1 [NOT SUPPORTED]", PORTNUMBER), error0.getDetail());
    }


    @Test
    public void test005_InvalidGetLocatie(){
        String uri =  BASE_URL +"/api/locatie/bla ";
        String msg = clientCallRequest(contructRequestGet(uri));
        assertNotNull(msg);
        System.out.println(msg);

        EnvelopeError result = getJsonObject(msg, EnvelopeError.class);
        assertNotNull(result);

        assertEquals(1,result.getErrors().size());
        JsonFormatError error0 = result.getErrors().get(0);
        assertEquals("500", error0.getStatus());
        assertEquals("url: " + uri.trim(), error0.getDetail() );
        String expectedError = "NumberFormatException For input string: \"bla\"";
        assertEquals(expectedError, error0.getTitle() );
    }

    @Test
    public void test006_findNolocatie() {
        final String uri = BASE_URL +"/api/locatie/4";

        String msg = clientCallRequest(contructRequestGet(uri));
        assertNotNull(msg);
        System.out.println(msg);

        EnvelopeError result = getJsonObject(msg, EnvelopeError.class);
        assertNotNull(result);

        assertEquals(1,result.getErrors().size());
        JsonFormatError error0 = result.getErrors().get(0);
        assertEquals("resource not found: id=4", error0.getTitle());
        assertEquals("404", error0.getStatus());
    }


    @Test
    public void test007_post404Error() {
        final String uri = BASE_URL +"/postdummy.json";
        HttpEntity<String> entity = new HttpEntity<>(headers);

        ResponseEntity<String> response = restTemplate.postForEntity(uri, entity, String.class);
        assertNotNull(response.getBody());
        String responstTxt = response.getBody();
        System.out.println(responstTxt);

        ErrorResponse result = getJsonObject(responstTxt, ErrorResponse.class);
        assertNotNull(result);
        assertEquals("No message available", result.getMessage());
        assertEquals(404, result.getStatus().intValue());
    }



    @Test
    public void test008_postNewLocatie(){
        JsonWrappedMessage jsonWrappedMessage = new JsonWrappedMessage();
        jsonWrappedMessage.setType("locatie");
        jsonWrappedMessage.getData().getAttributes().put("postcode","5201DZ");
        jsonWrappedMessage.getData().getAttributes().put("nummer",120);
        jsonWrappedMessage.getData().getAttributes().put("woonplaats","'s Hertogenbosch");
        jsonWrappedMessage.getData().getAttributes().put("straatnaam","Pettelaarpark");

        String uri =  BASE_URL +"/api/locatie";
        String msg = clientCallRequest(contructRequestPost(uri, jsonWrappedMessage));
        assertNotNull(msg);
        System.out.println(msg);

        JsonWrappedMessage result = getJsonObject(msg, JsonWrappedMessage.class);
        assertNotNull(result);
        assertEquals("locatie", result.getType());
        assertEquals("", result.getData().getAttributes().get("toevoeging"));
        assertEquals(4L, Long.valueOf(result.getId()).longValue());
    }

    @Test
    public void test009_PatchAsdresObjectOK(){
        String uri =  BASE_URL +"/api/locatie/1";
        ResponseEntity<JsonWrappedMessage> response0 = new RestTemplate().getForEntity(uri,  JsonWrappedMessage.class);
        JsonWrappedMessage orgAdresData = response0.getBody();

        orgAdresData.getData().getAttributes().put("telefoon", "12345678");
        orgAdresData.getData().getAttributes().put("nummer", 12);
        orgAdresData.getData().getAttributes().put("straatnaam", "Dubbelmolenerf");
        orgAdresData.getData().getAttributes().put("toevoeging", "");

        String msg = clientCallRequest(contructRequestPatch(uri,orgAdresData));
        assertNotNull(msg);
        JsonWrappedMessage result = getJsonObject(msg, JsonWrappedMessage.class);
        assertNotNull(result);
        assertEquals("12345678", result.getData().getAttributes().get("telefoon"));
        assertEquals(12, (result.getData().getAttributes().get("nummer")));
        assertEquals("Dubbelmolenerf", result.getData().getAttributes().get("straatnaam"));
    }

    @Test
    public void test009_PatchAsdresObjectToeVeogingNull(){
        String uri =  BASE_URL +"/api/locatie/1";
        ResponseEntity<JsonWrappedMessage> response0 = new RestTemplate().getForEntity(uri,  JsonWrappedMessage.class);
        JsonWrappedMessage orgAdresData = response0.getBody();

        orgAdresData.getData().getAttributes().put("telefoon", "12345678");
        orgAdresData.getData().getAttributes().put("nummer", 12);
        orgAdresData.getData().getAttributes().put("straatnaam", "Dubbelmolenerf");
        orgAdresData.getData().getAttributes().remove("toevoeging");

        String msg = clientCallRequest(contructRequestPatch(uri,orgAdresData));
        assertNotNull(msg);
        JsonWrappedMessage result = getJsonObject(msg, JsonWrappedMessage.class);
        assertNotNull(result);
        assertEquals("12345678", result.getData().getAttributes().get("telefoon"));
        assertEquals(12, (result.getData().getAttributes().get("nummer")));
        assertEquals("Dubbelmolenerf", result.getData().getAttributes().get("straatnaam"));
    }


    @Test
    public void test010_PatchAsdresObjectInvalidID(){
        String uri =  BASE_URL +"/api/locatie/1";
        ResponseEntity<JsonWrappedMessage> response0 = new RestTemplate().getForEntity(uri,  JsonWrappedMessage.class);
        JsonWrappedMessage orgAdresData = response0.getBody();

        orgAdresData.getData().getAttributes().put("telefoon", "12345678");
        orgAdresData.getData().getAttributes().put("nummer", 12);
        orgAdresData.getData().getAttributes().put("staatnaam", "Dubbelmolenerf");
        orgAdresData.getData().getAttributes().put("toevoeging", "");

        String InvaliudUri =  BASE_URL +"/api/locatie/999";

        String msg = clientCallRequest(contructRequestPatch(InvaliudUri,orgAdresData));
        assertNotNull(msg);

        EnvelopeError result = getJsonObject(msg, EnvelopeError.class);
        assertNotNull(result);
        assertEquals(1,result.getErrors().size());
        JsonFormatError error0 = result.getErrors().get(0);
        assertEquals("resource not found: id=999", error0.getTitle());
        assertEquals("404", error0.getStatus());
    }


    @Test
    public void test011_PatchAsdresObjectNotOK(){
        String uri =  BASE_URL +"/api/locatie/2";
        ResponseEntity<JsonWrappedMessage> response0 = new RestTemplate().getForEntity(uri,  JsonWrappedMessage.class);
        JsonWrappedMessage orgAdresData = response0.getBody();

        orgAdresData.getData().getAttributes().put("postcode", "5201DZ");
        orgAdresData.getData().getAttributes().put("nummer", 120);
        orgAdresData.getData().getAttributes().put("staatnaam", "Pettelaarpark");
        orgAdresData.getData().getAttributes().put("toevoeging", "");

        String msg = clientCallRequest(contructRequestPatch(uri,orgAdresData));
        assertNotNull(msg);

        EnvelopeError result = getJsonObject(msg, EnvelopeError.class);
        assertNotNull(result);
        assertEquals(1,result.getErrors().size());
        JsonFormatError error0 = result.getErrors().get(0);
        assertEquals("could not execute statement", error0.getTitle());
        assertEquals("500", error0.getStatus());
        assertTrue(error0.getDetail().startsWith("Unique index or primary key violation: "));
    }




    @Test  //(expected = ResourceAccessException.class)
    public void test100_ForClosingContext() {
        //curl -X POST localhost:8080/shutdownContext
        final String uri = BASE_URL +"/shutdownContext";

        expectedException.expect(ResourceAccessException.class);
        expectedException.expectMessage(String.format("I/O error on POST request for \"%s\"", uri));

        HttpEntity<String> entity = new HttpEntity<>(headers);
        restTemplate.postForEntity(uri, entity, String.class);
    }


    @Test
    public void test101_AfterClosingContext() {
        final String uri = BASE_URL +"/postdummy.json";

        expectedException.expect(ResourceAccessException.class);
        expectedException.expectMessage(String.format("I/O error on POST request for \"%s\"", uri));

        HttpEntity<String> entity = new HttpEntity<>(headers);
        restTemplate.postForEntity(uri, entity, String.class);
    }

    private JsonWrappedMessage handleGetLocatieDefinedIdByUrl(String uri){
        String msg = clientCallRequest(contructRequestGet(uri));
        assertNotNull(msg);
        JsonWrappedMessage result = getJsonObject(msg, JsonWrappedMessage.class);
        assertNotNull(result);
        return result;
    }
}