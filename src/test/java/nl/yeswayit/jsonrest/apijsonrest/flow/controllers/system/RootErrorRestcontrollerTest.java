package nl.yeswayit.jsonrest.apijsonrest.flow.controllers.system;

import nl.yeswayit.jsonrest.apijsonrest.testutils.TestRequestHttpHandler;
import nl.yeswayit.jsonrest.data.messages.error.ErrorResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.web.context.request.WebRequest;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RootErrorRestcontrollerTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(RootErrorRestcontrollerTest.class);
    @Mock
    public ErrorAttributes errorAttributes;

    @InjectMocks
    public RootErrorRestcontroller rootErrorRestcontroller;

    @Test
    public void error() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");

        Map<String, Object> errorInfo = new HashMap<>();
        errorInfo.put("timestamp", new Date());
        errorInfo.put("status", 404);
        errorInfo.put("error", "Not Found");
        errorInfo.put("message", "No message available");
        errorInfo.put("path", "/postdummy.json");
        WebRequest webRequest = mock(WebRequest.class);

        when(errorAttributes.getErrorAttributes(webRequest, false)).thenReturn(errorInfo);


        ErrorResponse errorResponse = rootErrorRestcontroller.error(webRequest);

        String errorResponseStr = TestRequestHttpHandler.getJsonAsString(errorResponse);
        LOGGER.info(errorResponseStr);
        assertEquals(errorInfo.get("status"), errorResponse.getStatus());
        assertEquals(errorInfo.get("error"), errorResponse.getError());
        assertEquals(errorInfo.get("message"), errorResponse.getMessage());
        assertEquals(errorInfo.get("path"), errorResponse.getPath());

    }

    @Test
    public void getErrorPath() {
        String errorPath = rootErrorRestcontroller.getErrorPath();
        assertEquals("/error", errorPath);
    }
}