package nl.yeswayit.jsonrest.data.annotations;

import nl.yeswayit.jsonrest.data.tables.BaseEntity;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface DvbJsonApiName {
    String value() default "";
}
