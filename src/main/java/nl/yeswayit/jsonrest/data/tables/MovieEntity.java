package nl.yeswayit.jsonrest.data.tables;

import nl.yeswayit.jsonrest.data.annotations.DvbJsonApiName;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Component
@DvbJsonApiName("movie")
@Entity
public class MovieEntity extends BaseEntity {
	private static final long serialVersionUID = -8156307977445388154L;

	@Id
	private UUID id;

	private String name;

	private int year;


	@OneToMany(mappedBy = "movie")
	private List<RoleEntity> roles = new ArrayList<>();

	@Version
	private Integer version;

	public MovieEntity() {
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public List<RoleEntity> getRoles() {
		return roles;
	}

	public void setRoles(List<RoleEntity> roles) {
		this.roles = roles;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}
}
