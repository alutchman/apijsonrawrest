package nl.yeswayit.jsonrest.data.tables;

import com.fasterxml.jackson.annotation.JsonProperty;
import nl.yeswayit.jsonrest.data.annotations.DvbJsonApiName;

import javax.persistence.*;
import javax.validation.constraints.Size;


@DvbJsonApiName("role")
@Entity
public class RoleEntity extends BaseEntity {

	private static final long serialVersionUID = 4157060967159581075L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	@JsonProperty("role")
	@Size(max = 50, message = "Name may not exceed {max} characters.")
	private String name;

	@Size(max = 50, message = "Description may not exceed {max} characters.")
	private String description;

	private Long someId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "movie_id")
	private MovieEntity movie;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "person_id")
	private PersonEntity person;

	public RoleEntity() {
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public MovieEntity getMovie() {
		return movie;
	}

	public void setMovie(MovieEntity movie) {
		this.movie = movie;
	}

	public Long getSomeId() {
		return someId;
	}

	public void setSomeId(Long someId) {
		this.someId = someId;
	}

	public PersonEntity getPerson() {
		return person;
	}

	public void setPerson(PersonEntity person) {
		this.person = person;
	}
}
