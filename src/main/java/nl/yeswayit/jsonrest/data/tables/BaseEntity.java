package nl.yeswayit.jsonrest.data.tables;


import com.fasterxml.jackson.annotation.JsonProperty;
import nl.yeswayit.jsonrest.data.annotations.DvbJsonApiName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ReflectionUtils;

import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.*;

@DvbJsonApiName("?base?")
@MappedSuperclass
public abstract class BaseEntity implements Serializable {
    private static final Logger LOGGER = LoggerFactory.getLogger(BaseEntity.class);
    private static final String LINKS_TXT = "links";
    private static final String SELF_TXT = "self";
    private static final String RELATED_TXT = "related";
    private static final String RELATIONSHIPS_TXT = "/relationships/";

    public final Object resloveEnitityId(){
        try {
            final Field field = this.getClass().getDeclaredField("id");
            field.setAccessible(true);
            Object result = ReflectionUtils.getField(field, this);
            field.setAccessible(false);

            return result;
        } catch (NoSuchFieldException e) {
            return null;
        }
    }

    public Map<String,Object> mapValues() {

        Map<String,Object> values = new HashMap<>();
        for (final Field field: this.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            if (field.getName().equals("id")
                    || field.getName().equals("serialVersionUID")
                    || (
                    !field.getType().isPrimitive() &&
                            !field.getType().isAssignableFrom(Long.class) &&
                            !field.getType().isAssignableFrom(String.class) &&
                            !field.getType().isAssignableFrom(Integer.class)
                    )
                    || field.isAnnotationPresent(ManyToOne.class)
                    || field.isAnnotationPresent(OneToMany.class)
                    ){
                continue;
            }
            try {
                JsonProperty jsonTypeInfo = field.getDeclaredAnnotation(JsonProperty.class);
                if (jsonTypeInfo != null) {
                    values.put(jsonTypeInfo.value(),ReflectionUtils.getField(field, this));
                } else {
                    values.put(field.getName(),ReflectionUtils.getField(field, this));
                }

            } catch (final Exception e) {

            }
            field.setAccessible(false);
        }
        return values;
    }


    public Map<String,Object> findRelations(String baseUrl) {

        Map<String,Object> relations = new HashMap<>();
        for (final Field field: this.getClass().getDeclaredFields()) {
            if (field.isAnnotationPresent(ManyToOne.class)) {
                field.setAccessible(true);
                Class fieldType = field.getType();
                if (BaseEntity.class.isAssignableFrom(fieldType)) {
                    Object relation = ReflectionUtils.getField(field, this);
                    BaseEntity relType = (BaseEntity) relation;
                    TreeMap<String,Object> entityRelations = new TreeMap<String,Object>(Collections.reverseOrder());
                    entityRelations.put(SELF_TXT, baseUrl + RELATIONSHIPS_TXT + relType.getType());
                    entityRelations.put(RELATED_TXT, baseUrl + "/" + relType.getType());

                    Map<String,Object> entityLinks = new HashMap<>();
                    entityLinks.put(LINKS_TXT, entityRelations);

                    relations.put(relType.getType(), entityLinks);
                }
                field.setAccessible(false);
            } else if (field.isAnnotationPresent(OneToMany.class)) {
                field.setAccessible(true);
                Class fieldType = field.getType();
                List<? extends BaseEntity> resultList = new ArrayList<>();

                Class fieldGenericType = getFieldGenericType(field);
                if (List.class.isAssignableFrom(fieldType)
                        && BaseEntity.class.getName().equals(fieldGenericType.getName()) ) {
                    resultList = (List<? extends BaseEntity>) ReflectionUtils.getField(field, this);
                    LOGGER.info("list size = {}", resultList.size());

                    TreeMap<String,Object> entityRelations  =new TreeMap<String,Object>(Collections.reverseOrder());

                    entityRelations.put(SELF_TXT, baseUrl + RELATIONSHIPS_TXT + field.getName());
                    entityRelations.put(RELATED_TXT, baseUrl + "/" +  field.getName());



                    Map<String,Object> entityLinks = new HashMap<>();
                    entityLinks.put(LINKS_TXT, entityRelations);
                    relations.put(field.getName(), entityLinks);
                }
                field.setAccessible(false);
            } else {
                continue;
            }
        }
        return relations.isEmpty() ? null : relations;
    }


    public String getType(){
        DvbJsonApiName  jsonTypeInfo = getClass().getAnnotation(DvbJsonApiName.class);
        return jsonTypeInfo.value();
    }

    /**
     * Returns generic type of any field
     *
     * @param field
     * @return
     */
    private Class getFieldGenericType(Field field) {
        if (ParameterizedType.class.isAssignableFrom(field.getGenericType().getClass())) {
            ParameterizedType genericType =
                    (ParameterizedType) field.getGenericType();
            return ((Class)
                    (genericType.getActualTypeArguments()[0])).getSuperclass();//getSuperclass
        }
        //Returns dummy Boolean Class to compare with ValueObject & FormBean
        return new Boolean(false).getClass();
    }



}
