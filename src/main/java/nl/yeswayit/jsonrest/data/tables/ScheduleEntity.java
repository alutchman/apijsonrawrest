package nl.yeswayit.jsonrest.data.tables;

import nl.yeswayit.jsonrest.data.annotations.DvbJsonApiName;

import javax.persistence.*;

@DvbJsonApiName("schedule")
@Entity
public class ScheduleEntity extends BaseEntity  {
	private static final long serialVersionUID = -7967615181040613673L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	private String name;

	@ManyToOne
	private MovieEntity movie;


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public MovieEntity getMovie() {
		return movie;
	}

	public void setMovie(MovieEntity movie) {
		this.movie = movie;
	}
}
