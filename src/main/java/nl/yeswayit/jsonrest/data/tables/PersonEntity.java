package nl.yeswayit.jsonrest.data.tables;

import nl.yeswayit.jsonrest.data.annotations.DvbJsonApiName;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@DvbJsonApiName("person")
@Entity
public class PersonEntity  extends BaseEntity {

	private static final long serialVersionUID = 2387905756210213435L;
	@Id
	private UUID id;

	private String name;

	private int year;

	@OneToMany(mappedBy = "person", cascade = CascadeType.ALL)
	private List<RoleEntity> roles = new ArrayList<>();

	@Version
	private Integer version;

	public PersonEntity() {
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public List<RoleEntity> getRoles() {
		return roles;
	}

	public void setRoles(List<RoleEntity> roles) {
		this.roles = roles;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}
}
