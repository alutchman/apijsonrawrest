package nl.yeswayit.jsonrest.data.tables;


import nl.yeswayit.jsonrest.data.annotations.DvbJsonApiName;

import javax.persistence.*;
import java.util.Objects;

@DvbJsonApiName("locatie")
@Entity
@Table(
        name="locaties",
        uniqueConstraints=
        @UniqueConstraint(columnNames={"postcode", "nummer", "toevoeging"})
)
public class Locaties extends BaseEntity  {
    private static final long serialVersionUID = -4667716247931256252L;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    protected Long id;

    @Column( name = "straatnaam",nullable = false, length = 60)
    private String straatnaam;

    @Column( name = "nummer",nullable = false)
    private Integer nummer;

    @Column( name = "toevoeging",nullable = false, length=10)
    private String toevoeging;

    @Column( name = "woonplaats",nullable = false, length = 25)
    private String woonplaats;

    @Column( name = "postcode",nullable = false, length = 8)
    private String postcode;

    @Column( name = "telefoon", length = 20)
    private String telefoon;


    public String getStraatnaam() {
        return straatnaam;
    }

    public void setStraatnaam(String straatnaam) {
        this.straatnaam = straatnaam;
    }

    public Integer getNummer() {
        return nummer;
    }

    public void setNummer(Integer nummer) {
        this.nummer = nummer;
    }

    public String getWoonplaats() {
        return woonplaats;
    }

    public void setWoonplaats(String woonplaats) {
        this.woonplaats = woonplaats;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getTelefoon() {
        return telefoon;
    }

    public void setTelefoon(String telefoon) {
        this.telefoon = telefoon;
    }


    public String getToevoeging() {
        return toevoeging;
    }

    public void setToevoeging(String toevoeging) {
        this.toevoeging = toevoeging;
    }



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Locaties locaties = (Locaties) o;
        return Objects.equals(getNummer(), locaties.getNummer()) &&
                Objects.equals(getToevoeging(), locaties.getToevoeging()) &&
                Objects.equals(getPostcode(), locaties.getPostcode());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPostcode());
    }


}
