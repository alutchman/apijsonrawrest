package nl.yeswayit.jsonrest.data.exception;

public class NotFoundIdException extends RuntimeException {
    private String id;
    private String url;


    public NotFoundIdException(String id, String url) {
        this.id = id;
        this.url = url;
    }

    public String getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }


}
