package nl.yeswayit.jsonrest.data.exception;

public class UnsupportedRelationException extends RuntimeException {
    private final String path;


    public UnsupportedRelationException(String relation , String path){
        super("The relation is invalid or not supported: "+ relation);
        this.path = path;
    }

    public String getPath() {
        return path;
    }

}
