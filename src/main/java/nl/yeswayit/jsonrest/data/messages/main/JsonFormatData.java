package nl.yeswayit.jsonrest.data.messages.main;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;

@JsonPropertyOrder({ "id",  "type", "attributes", "relationships", "links" })
public class JsonFormatData extends JsonRelation {

    private Map<String,Object> attributes = new HashMap<>();

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Map<String,Object> relationships ;

    private Map<String,Object> links;

    public Map<String, Object> getAttributes() {
        return attributes;
    }

    public void setAttributes(Map<String, Object> attributes) {
        this.attributes = attributes;
    }

    public Map<String, Object> getRelationships() {
        return relationships;
    }

    public void setRelationships(Map<String, Object> relationships) {
        this.relationships = relationships;
    }

    public Map<String, Object> getLinks() {
        return links;
    }

    public void setLinks(Map<String, Object> links) {
        this.links = links;
    }

    public void updateLink(String link){
        if (links == null) {
            links = new HashMap<>();
        }
        links.put("self", link);
    }


}
