package nl.yeswayit.jsonrest.data.messages.main;

import java.util.ArrayList;
import java.util.List;

public class JsonListMessage {
    private List<JsonFormatData> data = new ArrayList<>();

    public List<JsonFormatData> getData() {
        return data;
    }

    public void setData(List<JsonFormatData> data) {
        this.data = data;
    }
}
