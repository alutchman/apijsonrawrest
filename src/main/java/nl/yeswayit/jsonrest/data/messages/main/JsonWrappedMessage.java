package nl.yeswayit.jsonrest.data.messages.main;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(value = {"id", "type"})
public class JsonWrappedMessage {
    private JsonFormatData data = new JsonFormatData();

    public JsonFormatData getData() {
        return data;
    }

    public void setId(String id) {
        data.setId(id);
    }

    public void setType(String type) {
        data.setType(type);
    }

    public void setData(JsonFormatData data) {
        this.data = data;
    }

    public String getId() {
        return data.getId();
    }

    public String getType() {
        return data.getType();
    }
}
