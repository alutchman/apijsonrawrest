package nl.yeswayit.jsonrest.data.messages.error;

import java.util.ArrayList;
import java.util.List;

public class EnvelopeError {
    private List<JsonFormatError> errors = new ArrayList<>();

    public List<JsonFormatError> getErrors() {
        return errors;
    }

    public void setErrors(List<JsonFormatError> errors) {
        this.errors = errors;
    }

    public void addError(JsonFormatError error){
        errors.add(error);
    }
}
