package nl.yeswayit.jsonrest.data.messages.main;

public class JsonRelationData {
    private JsonRelation data = new JsonRelation();

    public JsonRelation getData() {
        return data;
    }

    public void setData(JsonRelation data) {
        this.data = data;
    }

}
