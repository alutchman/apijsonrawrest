package nl.yeswayit.jsonrest.data.messages.error;

import java.io.Serializable;

public class JsonFormatError implements Serializable {

    private static final long serialVersionUID = 7573460182274165873L;

    private String status;
    private String title;
    private String detail;


    public JsonFormatError(){

    }

    public JsonFormatError(String status, String title, String detail) {
        this();
        this.status = status;
        this.title = title;
        this.detail = detail;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
