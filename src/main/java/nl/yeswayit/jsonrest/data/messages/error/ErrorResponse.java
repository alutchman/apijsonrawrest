package nl.yeswayit.jsonrest.data.messages.error;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

public class ErrorResponse implements Serializable {
    private static final long serialVersionUID = 4484355824215402683L;

    private Integer status;
    private Date timestamp;
    private String error;
    private String message;
    private String path;

    public ErrorResponse(){

    }

    public ErrorResponse(Map<String, Object> info){
        info.forEach( (key,value) -> handleKv(key,value) );
    }

    private void handleKv(String key, Object value) {
        if (key.equals("timestamp")) {
            this.timestamp = (Date) value;
        } else if (key.equals("status")) {
            this.status = (Integer) value;
        } else if (key.equals("error")) {
            this.error = (String) value;
        } else if (key.equals("message")) {
            this.message = (String) value;
        } else if (key.equals("path")) {
            this.path = (String) value;
        }
    }

    public Integer getStatus() {
        return status;
    }

    public Date getTimestamp() {
        return timestamp;
    }


    public String getError() {
        return error;
    }


    public String getMessage() {
        return message;
    }


    public String getPath() {
        return path;
    }

}
