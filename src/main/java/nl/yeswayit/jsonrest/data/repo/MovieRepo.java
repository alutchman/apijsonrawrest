package nl.yeswayit.jsonrest.data.repo;


import nl.yeswayit.jsonrest.data.tables.MovieEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface MovieRepo extends JpaRepository<MovieEntity, UUID>{

}
