package nl.yeswayit.jsonrest.data.repo;

import nl.yeswayit.jsonrest.data.tables.RoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RoleRepo extends JpaRepository<RoleEntity, Long>{

    @Query("SELECT t FROM RoleEntity t where person.name=?1 ")
    Optional<List<RoleEntity>> findByActor(String personName);
}
