package nl.yeswayit.jsonrest.data.repo;


import nl.yeswayit.jsonrest.data.tables.PersonEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface PersonRepo extends JpaRepository<PersonEntity, UUID>{

}
