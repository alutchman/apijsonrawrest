package nl.yeswayit.jsonrest.data;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

public class WrapperResponse implements Serializable {
    private static final long serialVersionUID = 5797249407233776552L;
    private Date timestamp = new Date();

    private Object data;

    public Date getTimestamp() {
        return timestamp;
    }

    public Object getData() {
        return data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WrapperResponse that = (WrapperResponse) o;
        return Objects.equals(getTimestamp(), that.getTimestamp()) &&
                Objects.equals(getData(), that.getData());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getData());
    }

    public static class Builder {
        WrapperResponse wrapperResponse;

        public Builder withData(Object data){
            wrapperResponse = new WrapperResponse();
            wrapperResponse.data = data;
            return this;
        }

        public WrapperResponse build(){
            return wrapperResponse;
        }
    }

    public static WrapperResponse construct(Object  data){

        return new WrapperResponse.Builder().withData(data).build();

    }

}
