package nl.yeswayit.jsonrest.apijsonrest.flow.services;

import nl.yeswayit.jsonrest.apijsonrest.flow.utils.ResponseUtil;
import nl.yeswayit.jsonrest.data.exception.NotFoundIdException;
import nl.yeswayit.jsonrest.data.exception.UnsupportedRelationException;
import nl.yeswayit.jsonrest.data.messages.main.JsonFormatData;
import nl.yeswayit.jsonrest.data.messages.main.JsonListMessage;
import nl.yeswayit.jsonrest.data.messages.main.JsonWrappedMessage;
import nl.yeswayit.jsonrest.data.repo.RoleRepo;
import nl.yeswayit.jsonrest.data.tables.PersonEntity;
import nl.yeswayit.jsonrest.data.tables.RoleEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class RoleService implements RestEntityService {
    @Autowired
    private RoleRepo roleRepo;


    @Override
    public JsonWrappedMessage findEntity(String id, HttpServletRequest req) {
        Long rolId = Long.valueOf(id);
        Optional<RoleEntity> optEntity = roleRepo.findById(rolId);

        if (!optEntity.isPresent()){
            throw new NotFoundIdException(id, req.getRequestURL().toString());
        }

        RoleEntity rol = optEntity.get();

        JsonWrappedMessage jsonWrappedMessage = new JsonWrappedMessage();
        JsonFormatData response =  ResponseUtil.constructJsonFormatData(req.getRequestURL().toString(), rol);
        jsonWrappedMessage.setData(response);
        return jsonWrappedMessage;
    }

    @Override
    public Object findEntity(String id, HttpServletRequest req, String relation) {
        Long rolId = Long.valueOf(id);
        Optional<RoleEntity> optEntity = roleRepo.findById(rolId);

        if (!optEntity.isPresent()){
            throw new NotFoundIdException(id, req.getRequestURL().toString());
        }

        RoleEntity rol = optEntity.get();

        String initialUrl = req.getRequestURL().toString();
        String baseUrl = initialUrl;
        baseUrl = baseUrl.replaceAll("/"+relation, "");
        baseUrl = baseUrl.replaceAll("/relationships", "");
        baseUrl = baseUrl.replaceAll("/"+id, "");
        baseUrl = baseUrl.replaceAll("/"+rol.getType(), "");

        if (relation.equals("person")) {
            PersonEntity personEntity = rol.getPerson();
            baseUrl += "/"+relation+"/"+personEntity.getId().toString();
            JsonWrappedMessage jsonWrappedMessage = new JsonWrappedMessage();
            JsonFormatData response =  ResponseUtil.constructJsonFormatData(baseUrl, personEntity);
            jsonWrappedMessage.setData(response);
            return jsonWrappedMessage;
        }


        throw new UnsupportedRelationException(relation, initialUrl);
    }

    @Override
    public JsonListMessage findEntitiesByParam(HttpServletRequest req, Map<String, String> allRequestParams) {
        JsonListMessage jsonListMessage = new JsonListMessage();

        if (allRequestParams.containsKey("actor")) {
            String postcode = allRequestParams.get("actor");

            Optional<List<RoleEntity>> lstPostCode = roleRepo.findByActor(postcode);

            if (!lstPostCode.isPresent()){
                return jsonListMessage;
            }

            return ResponseUtil.createMessage(req.getRequestURL().toString()+"/", lstPostCode.get());
        }
        return jsonListMessage;
    }

    @Override
    public JsonWrappedMessage addEntityItem(JsonWrappedMessage entityAddRequest, HttpServletRequest req) {
        return null;
    }

    @Override
    public JsonWrappedMessage updateEntity(String id, JsonWrappedMessage jsonWrappedMessage, HttpServletRequest req) {
        return null;
    }

    @Override
    public void deleteEntityItem(String id, HttpServletRequest req) {

    }
}
