package nl.yeswayit.jsonrest.apijsonrest.flow.services;

import nl.yeswayit.jsonrest.data.messages.main.JsonListMessage;
import nl.yeswayit.jsonrest.data.messages.main.JsonWrappedMessage;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.util.Map;

public interface RestEntityService {

    JsonWrappedMessage findEntity(String id, HttpServletRequest req);

    Object findEntity(String id, HttpServletRequest req, String relation);

    JsonListMessage findEntitiesByParam(HttpServletRequest req, Map<String, String> allRequestParams);

    @Transactional
    JsonWrappedMessage addEntityItem(final JsonWrappedMessage entityAddRequest, HttpServletRequest req);

    @Transactional
    JsonWrappedMessage updateEntity(String id, JsonWrappedMessage jsonWrappedMessage, HttpServletRequest req);

    @Transactional
    void deleteEntityItem(String id, HttpServletRequest req);

}
