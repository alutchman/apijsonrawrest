package nl.yeswayit.jsonrest.apijsonrest.flow.controllers.system;


import nl.yeswayit.jsonrest.data.messages.error.ErrorResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;


@RestController
public class RootErrorRestcontroller  implements ErrorController {
    private static final String PATH = "/error";
    private boolean debug = false;

    @Autowired
    private ErrorAttributes errorAttributes;

    @RequestMapping(value = PATH)
    public ErrorResponse error(WebRequest webRequest) {
        Map<String, Object> errorInfo = errorAttributes.getErrorAttributes(webRequest, debug);
        final ErrorResponse errorResponse = new ErrorResponse(errorInfo);
        return errorResponse;
    }

    @Override
    public String getErrorPath() {
        return PATH;
    }
}
