package nl.yeswayit.jsonrest.apijsonrest.flow.services;

import nl.yeswayit.jsonrest.apijsonrest.flow.utils.ResponseUtil;
import nl.yeswayit.jsonrest.data.exception.NotFoundIdException;
import nl.yeswayit.jsonrest.data.exception.UnsupportedRelationException;
import nl.yeswayit.jsonrest.data.messages.main.JsonFormatData;
import nl.yeswayit.jsonrest.data.messages.main.JsonListMessage;
import nl.yeswayit.jsonrest.data.messages.main.JsonWrappedMessage;
import nl.yeswayit.jsonrest.data.repo.LocatiesRepo;
import nl.yeswayit.jsonrest.data.tables.Locaties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class LocatieService implements RestEntityService {
    private static final Logger LOGGER = LoggerFactory.getLogger(LocatieService.class);

    @Autowired
    private LocatiesRepo locatiesRepo;


    @Override
    public JsonWrappedMessage findEntity(String id, HttpServletRequest req) {
        Long locId = Long.valueOf(id);

        Optional<Locaties> optloc = locatiesRepo.findById(locId);
        if (!optloc.isPresent()){
            throw new NotFoundIdException(Long.toString(locId), req.getRequestURL().toString());
        }

        JsonWrappedMessage jsonWrappedMessage = new JsonWrappedMessage();
        Locaties loc = optloc.get();

        JsonFormatData response =  ResponseUtil.constructJsonFormatData(req.getRequestURL().toString(), loc);
        jsonWrappedMessage.setData(response);

        return jsonWrappedMessage;
    }

    @Override
    public JsonWrappedMessage findEntity(String id, HttpServletRequest req, String relation) {
        throw new UnsupportedRelationException(relation, req.getRequestURL().toString());
    }

    @Override
    public JsonListMessage findEntitiesByParam(HttpServletRequest req, Map<String, String> allRequestParams) {
        JsonListMessage jsonListMessage = new JsonListMessage();

        if (allRequestParams.containsKey("postcode")) {
            String postcode = "%"+ allRequestParams.get("postcode") + "%";
            Optional<List<Locaties>> lstPostCode = locatiesRepo.findLocatie(postcode);

            if (!lstPostCode.isPresent()){
                return jsonListMessage;
            }
            List<Locaties> itemsFound = lstPostCode.get();
            return ResponseUtil.createMessage(req.getRequestURL().toString()+"/", itemsFound);
        }
        return jsonListMessage;
    }

    /**
     *
     * @param entityAddRequest
     * @param req
     * @return
     */
    @Override
    @Transactional
    public JsonWrappedMessage addEntityItem(final JsonWrappedMessage entityAddRequest, HttpServletRequest req) {
        String toevoeging = (String) entityAddRequest.getData().getAttributes().get("toevoeging");
        if (toevoeging == null) {
            toevoeging = "";
        }
        int nummer = (Integer) entityAddRequest.getData().getAttributes().get("nummer");
        String postcode = (String) entityAddRequest.getData().getAttributes().get("postcode");
        String straatnaam = (String) entityAddRequest.getData().getAttributes().get("straatnaam");
        String telefoon = (String) entityAddRequest.getData().getAttributes().get("telefoon");
        String woonplaats =  (String) entityAddRequest.getData().getAttributes().get("woonplaats");

        Locaties locatie = new Locaties();
        locatie.setNummer(nummer);
        locatie.setPostcode(postcode);
        locatie.setStraatnaam(straatnaam);
        locatie.setTelefoon(telefoon);
        locatie.setToevoeging(toevoeging.trim());
        locatie.setWoonplaats(woonplaats);
        Locaties locSaved =locatiesRepo.save(locatie);

        JsonWrappedMessage jsonWrappedMessage = new JsonWrappedMessage();
        JsonFormatData response =  ResponseUtil.constructJsonFormatData(req.getRequestURL().toString(), locSaved);
        jsonWrappedMessage.setData(response);
        return  jsonWrappedMessage;
    }


    /**
     *
     * @param id
     * @param jsonWrappedMessage
     * @param req
     * @return
     */
    @Override
    @Transactional
    public JsonWrappedMessage updateEntity(String id, JsonWrappedMessage jsonWrappedMessage, HttpServletRequest req) {
        JsonWrappedMessage jsonWrappedResult = new JsonWrappedMessage();
        Long intId = Long.valueOf(id);

        Optional<Locaties> optloc = locatiesRepo.findById(intId);
        if (!optloc.isPresent()){
            throw new NotFoundIdException(Long.toString(intId), req.getRequestURL().toString());
        }

        Locaties loc = optloc.get();

        String toevoeging = (String) jsonWrappedMessage.getData().getAttributes().get("toevoeging");
        if (toevoeging == null) {
            toevoeging = "";
        }

        loc.setNummer((Integer) jsonWrappedMessage.getData().getAttributes().get("nummer"));
        loc.setPostcode((String) jsonWrappedMessage.getData().getAttributes().get("postcode"));
        loc.setToevoeging(toevoeging);
        loc.setStraatnaam((String) jsonWrappedMessage.getData().getAttributes().get("straatnaam"));
        loc.setTelefoon((String) jsonWrappedMessage.getData().getAttributes().get("telefoon"));
        loc.setWoonplaats((String) jsonWrappedMessage.getData().getAttributes().get("woonplaats"));



        JsonFormatData response =  ResponseUtil.constructJsonFormatData(req.getRequestURL().toString(), loc);
        jsonWrappedMessage.setData(response);
        return jsonWrappedMessage;
    }

    @Override
    @Transactional
    public void deleteEntityItem(String id, HttpServletRequest req) {
        Long locId = Long.valueOf(id);
        try {
            locatiesRepo.deleteById(locId);
        } catch(EmptyResultDataAccessException e) {
            throw new NotFoundIdException(Long.toString(locId), req.getRequestURL().toString());
        }
    }

}
