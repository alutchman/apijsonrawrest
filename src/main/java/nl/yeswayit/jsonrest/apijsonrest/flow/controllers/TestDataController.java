package nl.yeswayit.jsonrest.apijsonrest.flow.controllers;

import nl.yeswayit.jsonrest.apijsonrest.flow.services.RestEntityService;
import nl.yeswayit.jsonrest.data.messages.main.JsonWrappedMessage;
import nl.yeswayit.jsonrest.data.repo.LocatiesRepo;
import nl.yeswayit.jsonrest.data.repo.MovieRepo;
import nl.yeswayit.jsonrest.data.repo.PersonRepo;
import nl.yeswayit.jsonrest.data.repo.RoleRepo;
import nl.yeswayit.jsonrest.data.tables.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
public class TestDataController {
    private static final Logger LOGGER = LoggerFactory.getLogger(TestDataController.class);

    @Autowired
    private RoleRepo roleRepo;

    @Autowired
    private PersonRepo personRepo;

    @Autowired
    private MovieRepo movieRepo;

    @Autowired
    private LocatiesRepo locatiesRepo;

    @ResponseBody
    @RequestMapping(value = "/testData", method = RequestMethod.GET)
    public ResponseEntity<JsonWrappedMessage> findEntityByID(){
        JsonWrappedMessage jsonWrappedMessage = new JsonWrappedMessage();
        jsonWrappedMessage.setType("TEST");
        jsonWrappedMessage.setId("0");
        jsonWrappedMessage.getData().getAttributes().put("info", "aanmaken van testdata voor u");
        setup();
        return  new ResponseEntity<>(jsonWrappedMessage,HttpStatus.OK);
    }

    public void setup() {
        PersonEntity act01 = createPerson("Ben Affleck");
        PersonEntity act02 = createPerson("Anna Kendrick");
        PersonEntity act03 = createPerson("Robert Downey Jr.");
        PersonEntity act04 = createPerson("Stan Lee");
        PersonEntity act05 = createPerson("Jeff Bridges");
        PersonEntity act06 = createPerson("Brad Pitt");
        PersonEntity act07 = createPerson("Angelina Jolie");
        PersonEntity act08 = createPerson("Leonardo DiCaprio");
        PersonEntity act09 = createPerson("Kate Winslet");
        PersonEntity act10 = createPerson("Terrence Howard");

        createMovie("The Accountant", 2016, Arrays.asList(act01, act02));
        createMovie("Iron Man", 2008, Arrays.asList(act03, act10, act05));
        createMovie("Titanic", 1997, Arrays.asList(act08, act09));
        createMovie("Mr. & Mrs. Smith", 2005, Arrays.asList(act06, act07));

        createAdres("2803HA",10, "", "Gouda","Bleulandweg");
        createAdres("2491ES",49, "a", "Gouda","Burg van Willigenstraat");

    }

    @Transactional
    protected MovieEntity createMovie(String title, int year, List<PersonEntity> actors) {
        Optional<MovieEntity> optEntity = movieRepo.findById(UUID.nameUUIDFromBytes(title.getBytes()));
        if (optEntity.isPresent()){
            return optEntity.get();
        }

        MovieEntity movie = new MovieEntity();
        movie.setId(UUID.nameUUIDFromBytes(title.getBytes()));
        movie.setName(title);
        movie.setYear(year);
        movie = movieRepo.save(movie);
        movieRepo.flush();


        int nummer = 1;
        for (PersonEntity person : actors) {

            RoleEntity rol = new RoleEntity();
            rol.setPerson(person);
            rol.setMovie(movie);
            rol.setName("actor nr:"+ nummer++);
            rol.setDescription("actor: "+ person.getName());
            try {
                RoleEntity result = roleRepo.save(rol);
                roleRepo.flush();
                LOGGER.info("Role added with id = {}, tilte={}",result.getId() , rol.getDescription());

            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
            }
        }



        return movie;
    }

    @Transactional
    protected PersonEntity createPerson(String name) {
        Optional<PersonEntity> optEntity = personRepo.findById(UUID.nameUUIDFromBytes(name.getBytes()));

        if (optEntity.isPresent()){
            return optEntity.get();
        }

        PersonEntity  person = new PersonEntity();
        person.setId(UUID.nameUUIDFromBytes(name.getBytes()));
        person.setName(name);
        PersonEntity result = personRepo.save(person);
        personRepo.flush();

        LOGGER.info("Person added with id = {}, name={}",UUID.nameUUIDFromBytes(name.getBytes()).toString() , name);
        return result;
    }


    @Transactional
    protected Locaties createAdres(String postcode, int nummer, String toevoeging, String woonplaats, String straatnaam){
        Optional<Locaties> optEntity = locatiesRepo.findLocatie(postcode, nummer, toevoeging);

        if (optEntity.isPresent()){
            return optEntity.get();
        }

        Locaties locatie = new Locaties();
        locatie.setPostcode(postcode);
        locatie.setNummer(nummer);
        locatie.setToevoeging(toevoeging);
        locatie.setWoonplaats(woonplaats);
        locatie.setStraatnaam(straatnaam);
        Locaties addedLocatie =   locatiesRepo.save(locatie);
        locatiesRepo.flush();
        LOGGER.info("Location added with postcode = {}, nummer={}{}",postcode, nummer, toevoeging);

        return addedLocatie;
    }


}
