package nl.yeswayit.jsonrest.apijsonrest.flow.utils;

import nl.yeswayit.jsonrest.data.messages.main.JsonFormatData;
import nl.yeswayit.jsonrest.data.messages.main.JsonListMessage;
import nl.yeswayit.jsonrest.data.tables.BaseEntity;
import nl.yeswayit.jsonrest.data.tables.Locaties;

import java.util.List;
import java.util.Map;

public class ResponseUtil {

    public static<T extends BaseEntity> JsonListMessage createMessage(String baseUrl, List<T> itemsFound){
        JsonListMessage jsonListMessage = new JsonListMessage();
        for(BaseEntity baseEntity  : itemsFound) {
            JsonFormatData response = constructJsonFormatData(baseUrl, baseEntity, true);
            jsonListMessage.getData().add(response);
        }
        return jsonListMessage;
    }

    public static <T extends BaseEntity> JsonFormatData constructJsonFormatData(String baseUrl, T baseEntity){
        return constructJsonFormatData(baseUrl, baseEntity, false);
    }


    private static <T extends BaseEntity> JsonFormatData constructJsonFormatData(String baseUrl, T baseEntity, boolean addId){
        Object entityId = baseEntity.resloveEnitityId();
        String baseUrl2Use = addId ? baseUrl+"/"+entityId :  baseUrl;
        JsonFormatData response = new JsonFormatData();
        response.setId(String.valueOf(entityId));
        response.setType(baseEntity.getType());
        response.setAttributes(baseEntity.mapValues());

        Map<String,Object> relations = baseEntity.findRelations(baseUrl2Use);
        if (relations != null) {
            response.setRelationships(relations);
        }

        response.updateLink(baseUrl2Use);

        return response;
    }
}
