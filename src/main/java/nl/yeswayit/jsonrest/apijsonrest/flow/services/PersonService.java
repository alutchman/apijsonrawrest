package nl.yeswayit.jsonrest.apijsonrest.flow.services;

import nl.yeswayit.jsonrest.apijsonrest.flow.utils.ResponseUtil;
import nl.yeswayit.jsonrest.data.annotations.DvbJsonApiName;
import nl.yeswayit.jsonrest.data.exception.NotFoundIdException;
import nl.yeswayit.jsonrest.data.exception.UnsupportedRelationException;
import nl.yeswayit.jsonrest.data.messages.main.JsonFormatData;
import nl.yeswayit.jsonrest.data.messages.main.JsonListMessage;
import nl.yeswayit.jsonrest.data.messages.main.JsonWrappedMessage;
import nl.yeswayit.jsonrest.data.repo.PersonRepo;
import nl.yeswayit.jsonrest.data.tables.Locaties;
import nl.yeswayit.jsonrest.data.tables.PersonEntity;
import nl.yeswayit.jsonrest.data.tables.RoleEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@Service
public class PersonService  implements RestEntityService {

    @Autowired
    private PersonRepo personRepo;

    @Override
    public JsonWrappedMessage findEntity(String id, HttpServletRequest req) {
        Optional<PersonEntity> optEntity = personRepo.findById(UUID.fromString(id));
        if (!optEntity.isPresent()){
            throw new NotFoundIdException(id, req.getRequestURL().toString());
        }

        JsonWrappedMessage jsonWrappedMessage = new JsonWrappedMessage();
        PersonEntity personEntity = optEntity.get();

        JsonFormatData response =  ResponseUtil.constructJsonFormatData(req.getRequestURL().toString(), personEntity);
        jsonWrappedMessage.setData(response);
        return jsonWrappedMessage;
    }

    @Override
    public Object findEntity(String id, HttpServletRequest req, String relation) {
        Optional<PersonEntity> optEntity = personRepo.findById(UUID.fromString(id));
        if (!optEntity.isPresent()){
            throw new NotFoundIdException(id, req.getRequestURL().toString());
        }
        JsonWrappedMessage jsonWrappedMessage = new JsonWrappedMessage();
        PersonEntity personEntity = optEntity.get();

        String initialUrl = req.getRequestURL().toString();
        String baseUrl = initialUrl;
        baseUrl = baseUrl.replaceAll("/"+relation, "");
        baseUrl = baseUrl.replaceAll("/relationships", "");
        baseUrl = baseUrl.replaceAll("/"+id, "");
        baseUrl = baseUrl.replaceAll("/"+personEntity.getType(), "");

        if (relation.equals("roles")) {
            List<RoleEntity> itemsFound = personEntity.getRoles();

            DvbJsonApiName jsonTypeInfo = RoleEntity.class.getAnnotation(DvbJsonApiName.class);
            baseUrl += "/"+jsonTypeInfo.value();

            return ResponseUtil.createMessage(baseUrl, itemsFound);
        }
        throw new UnsupportedRelationException(relation, initialUrl);
    }

    @Override
    public JsonListMessage findEntitiesByParam(HttpServletRequest req, Map<String, String> allRequestParams) {
        JsonListMessage jsonListMessage = new JsonListMessage();

/*        if (allRequestParams.containsKey("postcode")) {
            String postcode = "%"+ allRequestParams.get("postcode") + "%";
            Optional<List<Locaties>> lstPostCode = personRepo.findById(postcode);

            if (!lstPostCode.isPresent()){
                return jsonListMessage;
            }
            List<Locaties> itemsFound = lstPostCode.get();
            return ResponseUtil.createMessage(req.getRequestURL().toString()+"/", itemsFound);
        }*/
        return jsonListMessage;
    }

    @Override
    public JsonWrappedMessage addEntityItem(JsonWrappedMessage entityAddRequest, HttpServletRequest req) {
        return null;
    }

    @Override
    public JsonWrappedMessage updateEntity(String id, JsonWrappedMessage jsonWrappedMessage, HttpServletRequest req) {
        return null;
    }

    @Override
    public void deleteEntityItem(String id, HttpServletRequest req) {

    }
}
