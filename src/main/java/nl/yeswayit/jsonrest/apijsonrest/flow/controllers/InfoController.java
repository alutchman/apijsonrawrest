package nl.yeswayit.jsonrest.apijsonrest.flow.controllers;


import nl.yeswayit.jsonrest.data.annotations.DvbJsonApiName;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.RegexPatternTypeFilter;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.regex.Pattern;

@RestController
public class InfoController {

    /**
     *
     * @param req
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public Map<String,Object> getResourceInfo(HttpServletRequest req) throws ClassNotFoundException {
        Map<String,Object> resourceInfo = new TreeMap<>();

        String path = req.getRequestURL().toString();//StringUtils.stripBack(req.getRequestURL().toString(), "/resources-info");

        // create scanner and disable default filters (that is the 'false' argument)
        final ClassPathScanningCandidateComponentProvider provider = new ClassPathScanningCandidateComponentProvider(false);

        // add include filters which matches all the classes (or use your own)
        provider.addIncludeFilter(new RegexPatternTypeFilter(Pattern.compile(".*")));

        // get matching classes defined in the package
        final Set<BeanDefinition> classes = provider.findCandidateComponents("nl.yeswayit.jsonrest.data.tables");

        // this is how you can load the class type from BeanDefinition instance
        for (BeanDefinition bean: classes) {
            Class<?> clazz = Class.forName(bean.getBeanClassName());

            DvbJsonApiName jsonTypeInfo = clazz.getAnnotation(DvbJsonApiName.class);
            if (jsonTypeInfo != null) {
                resourceInfo.put(jsonTypeInfo.value(), path+ "api/"+ jsonTypeInfo.value());
            }
        }

        Map<String,String> apiOptions = new TreeMap<>();
        apiOptions.put("POST /api/{entity}", "Add 1 entity item");
        apiOptions.put("GET  /api/{entity}/id", "Fetch 1 entity item");
        apiOptions.put("GET  /api/{entity}?param0=x,param1=y", "Fetch List of items based on params");
        apiOptions.put("DELETE /api/{entity}/id", "Delete 1 entity item");
        apiOptions.put("PATCH /api/{entity}/id", "Update 1 entity item");

        Map<String,Object> outerMap = new TreeMap<>();

        outerMap.put("entities", resourceInfo);
        outerMap.put("options", apiOptions);

        outerMap.put("resource-info", path);

        return outerMap;
    }
}
