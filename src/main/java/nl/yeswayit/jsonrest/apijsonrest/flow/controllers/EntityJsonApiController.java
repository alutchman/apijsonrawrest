package nl.yeswayit.jsonrest.apijsonrest.flow.controllers;


import nl.yeswayit.jsonrest.apijsonrest.flow.services.RestEntityService;
import nl.yeswayit.jsonrest.data.exception.NotFoundIdException;
import nl.yeswayit.jsonrest.data.exception.UnsupportedRelationException;
import nl.yeswayit.jsonrest.data.messages.error.EnvelopeError;
import nl.yeswayit.jsonrest.data.messages.error.JsonFormatError;
import nl.yeswayit.jsonrest.data.messages.main.JsonListMessage;
import nl.yeswayit.jsonrest.data.messages.main.JsonWrappedMessage;
import org.hibernate.JDBCException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RequestMapping("/api")
@RestController
public class EntityJsonApiController {
    private static final Logger LOGGER = LoggerFactory.getLogger(EntityJsonApiController.class);

    @Autowired
    ApplicationContext appcontext;

    /**
     *
     * @param entity
     * @param req
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/{entity}", method = RequestMethod.GET)
    public ResponseEntity<JsonListMessage> findItemsByParams(
            @PathVariable("entity") String entity,
            @RequestParam Map<String,String> allRequestParams,
            HttpServletRequest req){

        String serviceNaam = String.format("%sService", entity);
        RestEntityService restEntityService = (RestEntityService) appcontext.getBean(serviceNaam);
        JsonListMessage result = restEntityService.findEntitiesByParam(req,allRequestParams);

        return  new ResponseEntity<>(result, HttpStatus.OK);
    }


    /**
     *
     * @param entity
     * @param id
     * @param req
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/{entity}/{id}", method = RequestMethod.GET)
    public ResponseEntity<JsonWrappedMessage> findEntityByID(
            @PathVariable("entity") String entity,
            @PathVariable("id") String id,
            HttpServletRequest req){
        id = id.trim();
        String serviceNaam = String.format("%sService", entity);
        RestEntityService restEntityService = (RestEntityService) appcontext.getBean(serviceNaam);
        JsonWrappedMessage result = restEntityService.findEntity(id,req);
        return  new ResponseEntity<>(result,HttpStatus.OK);
    }




    @ResponseBody
    @RequestMapping(value = {"/{entity}/{id}/{relation}", "/{entity}/{id}/relationships/{relation}" }, method = RequestMethod.GET)
    public ResponseEntity<Object> findEntityByID_relation(
            @PathVariable("entity") String entity,
            @PathVariable("id") String id,
            @PathVariable("relation") String relation,
            HttpServletRequest req){
        id = id.trim();
        String serviceNaam = String.format("%sService", entity);
        RestEntityService restEntityService = (RestEntityService) appcontext.getBean(serviceNaam);
        Object result = restEntityService.findEntity(id,req, relation);
        return  new ResponseEntity<>(result,HttpStatus.OK);
    }



    /**
     *
     * @param entity
     * @param locatieAddRequest
     * @param req
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/{entity}", method = RequestMethod.POST)
    public JsonWrappedMessage handleAddEntity(
            @PathVariable("entity") String entity,
            @RequestBody final JsonWrappedMessage locatieAddRequest,
            HttpServletRequest req){
        String serviceNaam = String.format("%sService", entity);
        RestEntityService restEntityService = (RestEntityService) appcontext.getBean(serviceNaam);
        JsonWrappedMessage response = restEntityService.addEntityItem(locatieAddRequest,req);
        return response;
    }


    /**
     *
     * @param entity
     * @param id
     * @param jsonWrappedMessage
     * @param req
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/{entity}/{id}", method = RequestMethod.PATCH)
    public ResponseEntity<JsonWrappedMessage> updateEntity(
            @PathVariable("entity") String entity,
            @PathVariable("id") String id,
            @RequestBody JsonWrappedMessage jsonWrappedMessage,
            HttpServletRequest req){
        id = id.trim();
        String serviceNaam = String.format("%sService", entity);
        RestEntityService restEntityService = (RestEntityService) appcontext.getBean(serviceNaam);

        JsonWrappedMessage result = restEntityService.updateEntity(id,jsonWrappedMessage,req);
        return  new ResponseEntity<>(result,HttpStatus.OK);
    }


    /**
     *
     * @param entity
     * @param id
     * @param req
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/{entity}/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<JsonWrappedMessage> deletyById(
            @PathVariable("entity") String entity,
            @PathVariable("id") String id,
            HttpServletRequest req){
        id = id.trim();

        String serviceNaam = String.format("%sService", entity);
        RestEntityService restEntityService = (RestEntityService) appcontext.getBean(serviceNaam);
        restEntityService.deleteEntityItem(id,req);

        return  new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }


    /**
     *
     * @param ex
     * @return
     */
    @ExceptionHandler(NotFoundIdException.class)
    public ResponseEntity<EnvelopeError> handle(NotFoundIdException ex){
        JsonFormatError errorResponse = new JsonFormatError(Integer.toString(HttpStatus.NOT_FOUND.value()),
                "resource not found: id="+ ex.getId(), ex.getUrl());
        EnvelopeError envelopeError = new EnvelopeError();
        envelopeError.addError(errorResponse);
        return new ResponseEntity<>(envelopeError, HttpStatus.OK);
    }


    /**
     *
     * @param ex
     * @return
     */
    @ExceptionHandler(UnsupportedRelationException.class)
    public ResponseEntity<EnvelopeError> handleUnsupportedRelationException(UnsupportedRelationException ex){
        JsonFormatError errorResponse = new JsonFormatError(Integer.toString(HttpStatus.NOT_FOUND.value()),
                ex.getMessage(), "Endpoint: "+ex.getPath());
        EnvelopeError envelopeError = new EnvelopeError();
        envelopeError.addError(errorResponse);
        return new ResponseEntity<>(envelopeError, HttpStatus.OK);
    }


    //

    /**
     *
     * @param ex
     * @return
     */
    @ExceptionHandler(JDBCException.class)
    public ResponseEntity<EnvelopeError> jdbcSqlException(JDBCException ex){
        JsonFormatError errorResponse = new JsonFormatError(Integer.toString(HttpStatus.INTERNAL_SERVER_ERROR.value()),
                ex.getMessage(),ex.getSQLException().getMessage());

        EnvelopeError envelopeError = new EnvelopeError();
        envelopeError.addError(errorResponse);
        return new ResponseEntity<>(envelopeError, HttpStatus.OK);
    }


    /**
     *
     * @param ex
     * @param req
     * @return
     */
    @ExceptionHandler(NumberFormatException.class)
    public ResponseEntity<EnvelopeError> numberFromatException(NumberFormatException ex, HttpServletRequest req){
        JsonFormatError errorResponse = new JsonFormatError(Integer.toString(HttpStatus.INTERNAL_SERVER_ERROR.value()),
                NumberFormatException.class.getSimpleName() + " " + ex.getMessage(),
                "url: "+ req.getRequestURL().toString());
        EnvelopeError envelopeError = new EnvelopeError();
        envelopeError.addError(errorResponse);
        return new ResponseEntity<>(envelopeError, HttpStatus.OK);
    }


    /**
     *
     * @param ex
     * @param req
     * @return
     */
    @ExceptionHandler(NoSuchBeanDefinitionException.class)
    public ResponseEntity<EnvelopeError> noSuchBeanDefinitionException(NoSuchBeanDefinitionException ex, HttpServletRequest req){
        JsonFormatError errorResponse = new JsonFormatError(Integer.toString(HttpStatus.INTERNAL_SERVER_ERROR.value()),
                 ex.getMessage(),
                "url: "+ req.getRequestURL().toString() + " [NOT SUPPORTED]");
        EnvelopeError envelopeError = new EnvelopeError();
        envelopeError.addError(errorResponse);
        return new ResponseEntity<>(envelopeError, HttpStatus.OK);
    }

}
